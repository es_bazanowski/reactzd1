import React from 'react';

const List = (props) => {
    const foodies = [props.name];
    const foodList = foodies.map((food)=>
    <li>{food}</li>)
    return(
        
      
             <ul>{foodList}</ul>
             
    );
}


// class List extends React.Component {
//     constructor(props) {
//     super(props)
//     }
//     render()
    
//     {
//         const foodies = [this.props.name];
//         const foodList = foodies.map((food)=>
//         <li>{food}</li>)
//         return <ul>{foodList}</ul>
//     }
    
// }

export default List;